\subsection{Two Ingredients for Performance Portability}

\noindent Recent independent efforts to reconcile the different definitions of ``performance portability'' found in the literature~\cite{Larkin-PP,Zhu-Earth,SMS-Grid,Kokkos} have produced two strikingly similar definitions, suggesting that the community may be approaching consensus:

\blockquote{\textbf{Definition 1}~\cite{PP-Metric,PP-Metric-Implications}\\A measurement of an application's performance efficiency for a given problem that can be executed correctly on all platforms in a given set, where ``performance efficiency'' is defined as a fraction of peak theoretical hardware performance (\emph{architectural efficiency}) or best observed performance (\emph{application efficiency}).}

\blockquote{\textbf{Definition 2}~\cite{PP-Website}\\An application is performance portable if it achieves a consistent ratio of the actual time to solution to either the best-known or the theoretical best time to solution on each platform with minimal platform-specific code required.}

\noindent That the second definition suggests developers should minimize the amount of platform-specific code in an application reflects a desire for a productive solution to performance portability.  We therefore identify two ingredients for performance portability: 1) the ability of an application to achieve a consistent level of performance across the platforms that it targets; and 2) the ability to achieve this level of performance productively.

Whether differences in source code is a good measure of developer effort/productivity remains a point of contention and is beyond the scope of this paper~\cite{Wienke-EffortEstimation}.  Instead, we focus on the first ingredient of performance portability, which we refer to henceforth as ``performance consistency'' to avoid confusion.

\subsection{Quantitative Metric to Define Performance Consistency}

\noindent Another similarity between the efforts in~\cite{PP-Metric,PP-Metric-Implications,PP-Website} is the conclusion that the community needs to adopt quantitative metrics tied to the accepted definition of performance portability.  This goal is worth pursuing even if the metrics are imperfect, as they still have significant value: helping to frame discussions between different groups of researchers; facilitating meaningful comparisons between different software solutions and research efforts; and guiding developer efforts to achieve performance portability across their platforms of interest.

With this in mind, we propose a metric for performance consistency that builds upon the performance portability metric from~\cite{PP-Metric,PP-Metric-Implications}.  Specifically, for a given set of platforms $H$, the performance portability \ppm of an application $a$ solving problem $p$ is defined as:
\[
 \pp(a,p,H) =
 \begin{cases}
  \dfrac{|H|}{\sum_{i \in H} \dfrac{1}{e_i(a,p)}} & \text{if } i \text{ is supported } \forall i \in H \\
  0                                             & \text{otherwise} \\
 \end{cases}
\]

\noindent where the performance efficiency $e_i(a,p)$ of application $a$ solving problem $p$ on platform $i$, is calculated using an appropriate performance bottleneck as chosen by the Roofline model~\cite{Roofline}:
\[
  e_i(a,p) = \dfrac{P(a,p)}{\min(F_i, B_i \times I_i(a,p))}
\]
\noindent where $P_i(a,p)$ is observed performance, $F_i$ is peak floating-point performance, $B_i$ is peak bandwidth and $I_i(a,p)$ is arithmetic (or operational) intensity.  Note that this formulation permits arithmetic intensity to differ across platforms and problem sizes, to account for applications which run different code-paths or algorithms specialized for different devices or inputs.

\subsection{Meaningful Efficiency Measurements}

\noindent Architectural efficiency is an important complement to application efficiency, because it can demonstrate a gap between the best-observed performance and the peak that hardware is capable of delivering---this is particularly useful when application efficiency is 100\%.  However, unlike application efficiency, which admits no choice in what to measure (\emph{i.e.} there is a unique `best' performance measurement on a platform, and that becomes the baseline), architectural efficiency can be computed in a variety of ways.

\paragraph{Bottlenecks} Hardware resource bottlenecks govern the performance of all software, and each potential bottleneck can be analyzed in the context of an application, yielding a measurement of:
\[
  \textrm{architectural efficiency} = \dfrac{\textrm{achieved performance}}{\textrm{peak performance}}
\]
for that bottleneck/application pair.  The venerable \emph{FLOP/s} is one of the most common bottlenecks analyzed in HPC; \emph{main memory bandwidth} is another.  Other common bottlenecks used in analysis are \emph{network injection bandwidth} and \emph{cache bandwidth}.

When computing architectural efficiency, it is common to use the same bottleneck for each application being compared. This is what is done for the efficiencies reported in the Top500 results~\cite{top500} (FLOP/s) and the HPCG results~\cite{hpcg} (FLOP/s and main memory bandwidth).  This methodology certainly yields interesting results, but it is also true that these are synthetic benchmarks, made of deliberately simple kernels with weak-scaling problems applied.

\paragraph{Per-architecture Bounds} Real applications---particularly those running realistic problems on diverse hardware---may benefit from choosing \emph{different} bounds for each architecture when computing application efficiency. For example, a stencil code on an architecture with small caches and high-bandwidth memory may run best streaming to and from main memory, while an architecture with large caches and lower-bandwidth memory may benefit from a cache blocking scheme that is compute-bound. For the former case, the main memory bandwidth is the bottleneck, and for the latter, the peak FLOP/s of the system may be the bottleneck. Using the most relevant bottleneck to evaluate each architecture independently allows us to compute architectural efficiencies that reward methods that utilize features particular to each design.

% BW-bound implementation
% Cache-blocked "

\begin{table}
\footnotesize
\caption{A worked example of computing application efficiency relative to the appropriate Roofline bound.\label{tab:roofline-eff-perf}}
\subfloat[][Synthetic performance numbers for two implementations (I \& II) run on two platforms (A \& B).\label{tab:roofline-eff-perf}]{\begin{tabular}{r|r|r|r|r|r|r}
  \multirow{2}{*}{\textbf{Plat.}} & \multirow{2}{*}{\textbf{Impl.}} & \multicolumn{2}{c|}{\textbf{FLOP/s}} & \multicolumn{2}{c|}{\textbf{BW (GB/s)}} & \multirow{2}{*}{\textbf{App. Perf.}\footnotemark[1]} \\
  \cline{3-6}
                                  &                                 & \textbf{Obs.}                         & \textbf{Eff.}                            & \textbf{Obs.} & \textbf{Eff.} &                      \\
  \hline
  \multirow{2}{*}{A}              & I                               & 100                                   & 13\%                                     & 70            & 78\%          & 10                   \\
                                  & II                              & 500                                   & 63\%                                     & 30            & 33\%          & 20                   \\
  \hline
  \multirow{2}{*}{B}              & I                               & 150                                   & 13\%                                     & 160           & 80\%          & 25                   \\
                                  & II                              & 80                                    & 7\%                                      & 50            & 25\%          & 4                    \\
  \hline
  \multicolumn{7}{r}{\footnotemark[1] Higher is better.}\\
\end{tabular}}

\subfloat[][Computing \ppm{} for all combinations of the above results. Note that only the highlighted columns use results that deliver the actual peak application performance for their respective platforms.\label{tab:roofline-eff-pp}]{\begin{tabular}{p{0.6cm}|p{1.0cm}|p{1.0cm}|p{0.6cm}|p{1.0cm}|p{1.0cm}|p{0.5cm}}
  \multicolumn{3}{c|}{\textbf{A}} & \multicolumn{3}{c|}{\textbf{B}} &                                                                                               \\
  \hline
  \textbf{Impl.}                  & \textbf{FLOP/s eff.}            & \textbf{BW eff.} & \textbf{Impl.} & \textbf{FLOP/s eff.} & \textbf{BW eff.} & \textbf{\ppm{}} \\
  \hline
  I                               & \textbf{13\%}                   & 78\%             & I              & \textbf{13\%}        & 80\%             & 13\%            \\
  I                               & \textbf{13\%}                   & 78\%             & II             & \textbf{7\%}         & 25\%             & 9\%             \\
  II                              & \textbf{63\%}                   & 33\%             & II             & \textbf{7\%}         & 25\%             & 12\%            \\
\rowcolor{lightgreen}  II         & \textbf{63\%}                   & 33\%             & I              & \textbf{13\%}        & 80\%             & 21\%            \\
  I                               & \textbf{13\%}                   & 78\%             & I              & 13\%                 & \textbf{80\%}    & 22\%            \\
  I                               & \textbf{13\%}                   & 78\%             & II             & 7\%                  & \textbf{25\%}    & 17\%            \\
  II                              & \textbf{63\%}                   & 33\%             & II             & 7\%                  & \textbf{25\%}    & 36\%            \\
\rowcolor{lightgreen}  II         & \textbf{63\%}                   & 33\%             & I              & 13\%                 & \textbf{80\%}    & 70\%            \\
  I                               & 13\%                            & \textbf{78\%}    & I              & 13\%                 & \textbf{80\%}    & 79\%            \\
  I                               & 13\%                            & \textbf{78\%}    & II             & 7\%                  & \textbf{25\%}    & 38\%            \\
  II                              & 63\%                            & \textbf{33\%}    & II             & 7\%                  & \textbf{25\%}    & 29\%            \\
\rowcolor{lightgreen}  II         & 63\%                            & \textbf{33\%}    & I              & 13\%                 & \textbf{80\%}    & 47\%            \\
  I                               & 13\%                            & \textbf{78\%}    & I              & \textbf{13\%}        & 80\%             & 22\%            \\
  I                               & 13\%                            & \textbf{78\%}    & II             & \textbf{7\%}         & 25\%             & 12\%            \\
  II                              & 63\%                            & \textbf{33\%}    & II             & \textbf{7\%}         & 25\%             & 11\%            \\
\rowcolor{lightgreen}  II         & 63\%                            & \textbf{33\%}    & I              & \textbf{13\%}        & 80\%             & 18\%            \\
  \hline
\end{tabular}}\end{table}

\paragraph{Pitfalls} The complexity of modern hardware is such that the list of meaningful bottlenecks to analyze is practically limitless---one can concentrate on the throughput of instruction decoding, the utilized capacity of on-die meshes, page translation throughput, and so on. Even common metrics like FLOP/s and main memory bandwidth are nuanced; consider that single-precision FLOP/s are typically higher than double-precision, or that main memory bandwidth typically depends on the number of streams being read and written.  Furthermore, it can be misleading to consider architectural efficiency without considering application efficiency or some other application-specific metric.

Consider the aforementioned (synthetic) stencil example. Call the small-cache, high-bandwidth main memory architecture ``A'' and the large-cache, lower-bandwidth main memory architecture ``B''. The bandwidth-bound implementation (I) of the stencil kernel in Table~\ref{tab:roofline-eff-perf} is relatively efficient when considering main-memory bandwidth on both architectures, and it drops for the blocked implementation (II) on both architectures. Table~\ref{tab:roofline-eff-pp} shows all possible \ppm{} scores that can be computed from this data, typing different combinations of implementations and bottleneck metric. If we paid no mind to the actual application-defined throughput of the various results, we might be tempted to use implementation I with the main memory bandwidth efficiency metric, which yields a \ppm value of 79\%.

If we instead consider the implementation with the best performance on each platform, using conventional architectural efficiency yields a \ppm score of 21\% (if we use FLOP/s) or 47\% (if we use bandwidth).  Calculating the efficiency relative to the most appropriate bound for each platform/implementation pairing, we observe a \ppm score of 70\%. This represents the best performance we have on each platform, and is the most meaningful of our options when basing our results on architectural efficiency.

These measurements are all independent of an application's source code and thereby the approach it uses to achieve portability---the same performance consistency metric can be used for single-source applications developed in portable or domain-specific languages, applications that leverage platform-specific libraries, or application suites consisting of application binaries.  This flexibility allows for the metric to be used to compare applications with different developmental approaches, but more importantly ensures that the metric can be meaningfully tracked over time: improvements in performance consistency always correspond to improvements in performance on one or more platforms of interest.

In the remainder of this paper, we detail a methodology for measuring performance consistency, and apply it to a number of benchmark applications.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "PerformanceConsistency"
%%% End:
