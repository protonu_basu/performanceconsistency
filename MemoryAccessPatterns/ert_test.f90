program ert_memory_patterns

  implicit none

  real(kind(1.0d0)), allocatable :: a(:,:,:), b(:,:,:), c(:,:,:), cs
  real(kind(1.0d0)) :: starttime, endtime, bytes, runtimea, runtimeb, runtimec

  integer :: n, m
  integer :: i, j, k, nthreads, tid, OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM

  CHARACTER(len=32) :: arg

  CALL getarg(1, arg)
  READ(arg,*) n

  CALL getarg(2, arg)
  READ(arg,*) m

!$OMP PARALLEL PRIVATE(tid)
      tid = OMP_GET_THREAD_NUM()
      IF (tid .EQ. 0) THEN
        nthreads = OMP_GET_NUM_THREADS()
        PRINT *, 'Number of threads = ', nthreads
        PRINT *, 'n = ', n
        PRINT *, 'm = ', m
      END IF
!$OMP END PARALLEL

  ALLOCATE(a(n,m,nthreads))
  ALLOCATE(b(n,m,nthreads))
  !ALLOCATE(c(n,m,nthreads))
  
!$OMP PARALLEL DO private(j,i,k)
  do k = 1, nthreads
    do j = 1, m
      do i = 1, n
        a(i,j,k)=1D0
        b(i,j,k)=2D0
        !c(i,j,k)=0D0
      enddo
    enddo
  enddo

  bytes=8D0*n*m*nthreads*1D0/1000/1000/1000

  call timget(starttime)
  
!$OMP PARALLEL DO private(j,i,k) reduction(+:cs)
  do k = 1, nthreads
  do i = 1, m
    do j = 1, n
!      c(j,i,k) = a(j,i,k) * b(j,i,k)
      cs = cs + a(j,i,k) * b(j,i,k)
    enddo
  enddo
  enddo

  call timget(endtime)

  runtimea=endtime-starttime

  write(*,*) "Bytes read:", 2*bytes
  !write(*,*) "Bytes write:", bytes
  write(*,*) "Contiguous Runtime", runtimea
  write(*,*) "Contiguous Perf", 2*bytes/runtimea


  DEALLOCATE(b)
  ALLOCATE(b(m,n,nthreads))

!$OMP PARALLEL DO private(j,i,k) reduction(+:cs)
  do k = 1, nthreads
    do j = 1, m
      do i = 1, n
        a(i,j,k)=1D0
        b(j,i,k)=2D0
        !c(i,j,k)=0D0
      enddo
    enddo
  enddo


  call timget(starttime)

!$OMP PARALLEL DO private(j,i,k) reduction(+:cs)
  do k = 1, nthreads
    do j = 1, m
      do i = 1, n
        !c(i,j,k) = a(i,j,k) * b(j,i,k)
        cs = cs + a(i,j,k) * b(j,i,k)
    enddo
  enddo
  enddo

  call timget(endtime)

  runtimeb=endtime-starttime

  runtimec=runtimeb-(runtimea/2D0)

  write(*,*) "Strided Runtime", runtimeb
  write(*,*) "Strided Perf", 2*bytes/runtimeb
  write(*,*) "Strided Perf Corrected", bytes/runtimec 

end program

subroutine timget(wall)
  real(kind(1.0d0)) :: wall

  integer :: values(8)

  call date_and_time(VALUES=values)
  wall=((values(3)*24.0d0+values(5))*60.0d0 &
    +values(6))*60.0d0+values(7)+values(8)*1.0d-3
  return
end subroutine timget

